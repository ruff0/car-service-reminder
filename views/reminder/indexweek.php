<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
// use app\models\Status;
use app\models\Reminder;
use app\models\ReminderSearch;

?>
<?php $tituloexport = 'Recordatorios de los siguientes 7 dias: '.$today?>
<?php $this->title = $tituloexport?>
<div class="reminder-indextoday">

<?php $gridColumns =  [
    [
      'attribute'=>'next_date',
      'vAlign'=>'middle',
      'hAlign'=>'right',
      'width'=>'9%',
      'format'=>'date',
      'pageSummary'=>false
    ],
    [
    'attribute'=>'customer',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Cliente',

    ],
    [
    'attribute'=>'phone',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Teléfono',

    ],
    [
    'attribute'=>'car',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Automóvil',

    ],
    [
    'attribute'=>'service',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Servicio',
    'value' => function ($model) {
          if ($model->service == 1) {return 'Cambio de Aceite';}
          if ($model->service == 2) {return 'Afinación';}
          if ($model->service == 3) {return 'Lavado de Inyectores';}
    }
    ],
    [
    'attribute'=>'anotation',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Comentarios',

    ],

    [
      'attribute'=>'date',
      'vAlign'=>'middle',
      'hAlign'=>'right',
      'label' => 'Servicio Anterior',
      'width'=>'9%',
      'format'=>'date',
      'pageSummary'=>false
    ],

    [
              'class' => 'kartik\grid\ActionColumn',
              'header'=>false,
              // 'pageSummary' => false,
              'options'=>['style'=>'width:150px;'],
              'buttonOptions'=>['class'=>'btn btn-default'],
              'template'=>'{view}',
              'buttons'=>[

                'view'=> function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, [
                                        'title' => Yii::t('yii', 'Viewtoday'),
                                ]);
                              }
              ]
            ],

]?>

<?= GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'columns'=>$gridColumns,
    // 'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
    // 'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    // 'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>true, // pjax is set to always true for this demo
    // set your toolbar
    'export' => false,
    'toggleData' => false,
    // parameters from the demo form
    'bordered'=>false,
    'striped'=>true,
    // 'condensed'=>true,
    'responsive'=>true,
    'hover'=>false,
    'showPageSummary'=>false,
    'panel'=>[
        'type'=>GridView::TYPE_DEFAULT,
        'heading'=>'Recordatorios de la Semana',
    ],
    'persistResize'=>true,
    // 'toggleDataOptions'=>['minCount'=>10],
]);?>

</div>
