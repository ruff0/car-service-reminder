<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReminderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
//
// $this->title = 'Reminders';
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="reminder-index">
<div class="pull-right">
    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Registrar Servicio', ['create'], ['class' => 'btn btn-default']) ?>
    </p>
  </div>


<?php $gridColumns =  [


      [
        'attribute'=>'date',
        'vAlign'=>'middle',
        'hAlign'=>'right',
        'label' => 'Fecha del Servicio',
        'width'=>'9%',
        'format'=>'date',
        'pageSummary'=>false
      ],
    [
    'attribute'=>'customer',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Cliente',

    ],
    [
    'attribute'=>'phone',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Teléfono',

    ],
    [
    'attribute'=>'car',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Automóvil',

    ],
    [
    'attribute'=>'service',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Servicio',
    'value' => function ($model) {
          if ($model->service == 1) {return 'Cambio de Aceite';}
          if ($model->service == 2) {return 'Afinación';}
          if ($model->service == 3) {return 'Lavado de Inyectores';}
    }

    ],
    [
    'attribute'=>'anotation',
    'width'=>'310px',
    'pageSummary'=>false,
    'label' => 'Comentarios',

    ],

    [
      'attribute'=>'next_date',
      'vAlign'=>'middle',
      'hAlign'=>'right',
      'width'=>'9%',
      'format'=>'date',
      'pageSummary'=>false,
      'label' => 'Siguiente Servicio',

    ],

    [
              'class' => 'kartik\grid\ActionColumn',
              'header'=>false,
              // 'pageSummary' => false,
              'options'=>['style'=>'width:150px;'],
              'buttonOptions'=>['class'=>'btn btn-default'],
              'template'=>'{view}',
              'buttons'=>[

                'view'=> function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-plus"></span>', $url, [
                                        'title' => Yii::t('yii', 'Viewtoday'),
                                ]);
                              }
              ]
            ],

]?>

<?= GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'columns'=>$gridColumns,

    // 'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
    // 'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    // 'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>true, // pjax is set to always true for this demo
    // set your toolbar
    'export' => false,
    'toggleData' => false,
    // parameters from the demo form
    'bordered'=>false,
    'striped'=>true,
    // 'condensed'=>true,
    'responsive'=>true,
    'hover'=>false,
    'summary' => false,
    'showPageSummary'=>false,
    'panel'=>[
        'type'=>GridView::TYPE_INFO,
        'heading'=>'Servicios Registrados',
    ],
    'persistResize'=>true,
    // 'toggleDataOptions'=>['minCount'=>10],
]);?>

</div>
