<?php

use yii\helpers\Html;
// use yii\widgets\DetailView;
use kartik\detail\DetailView;
use yii2assets\printthis\PrintThis;

// $this->title = $model->id;
// $this->params['breadcrumbs'][] = ['label' => 'Reminders', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="reminder-view">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

<?php
    if ($model->service == 1) {$service = 'Cambio de Aceite';}
    if ($model->service == 2) {$service = 'Afinación';}
    if ($model->service == 3) {$service = 'Lavado de Inyectores';}
?>


    <?= DetailView::widget([
        'model' => $model,
        'mode'=> 'view',
        'bordered' => false,
        'striped' => true,
        'condensed' => false,
        'responsive' => true,
        'hover' => false,
        'bootstrap' => true,
        'attributes' => [
            // 'id',
            'customer',
            'car',
            'phone',
            ['attribute' => 'service',
            'value' => $service,
            ],

            'anotation',
            ['attribute' => 'date', 'format' => 'date'],
            ['attribute' => 'next_date', 'format' => 'date'],

            // 'permutation',
            // 'test',
        ],
    ]) ?>

    <p class="pull-right">
      <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
      <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
          'confirm' => 'Seguro que desea eliminar esto?',
          'method' => 'post',
        ],
        ]) ?>
      </p>

</div>
<div style="display: none;">
<div id="PrintThis">


<table>
  <tr>
    <td>
<div class="col-md-4" >
  <h2> REFACCIONARIA Y SERVICIOS JUÁREZ</h2>
</div>
    </td>
    <td>
<div class="col-md-2">
  <img style="width:64px" src="/img/logo.jpg">
</div>
    </td>
  </tr>
  <tr>
    <td>
<div style="text-align:center">
<h3> Tels: 323 1223 , 312 143 5680, 312 165 0313</h3>
</div>
    </td>
  </tr>
</table>
      <?= DetailView::widget([
          'model' => $model,
          'labelColOptions' => ['style' => 'width:30%'],
          'mode'=> 'view',
          'bordered' => false,
          'striped' => true,
          'condensed' => true,
          'responsive' => true,
          'hover' => true,
          'bootstrap' => true,
          'hAlign' => 'right',
          'attributes' => [
              // 'id',
              // 'customer',
              'car',
              // 'phone',
              ['attribute' => 'service',
              'value' => $service,
              ],
              ['attribute' => 'anotation', 'label' => 'Anotaciones'],
              ['attribute' => 'date', 'format' => 'date', 'label' => 'Fecha del Último Servicio'],
              ['attribute' => 'next_date', 'format' => 'date', 'label' => 'Fecha del Siguiente Servicio'],
          ],
      ]) ?>
</div>
</div>


<?php
echo PrintThis::widget([
	'htmlOptions' => [
		'id' => 'PrintThis',
		'btnClass' => 'btn btn-info',
		'btnId' => 'btnPrintThis',
		'btnText' => 'Imprimir',
		'btnIcon' => 'fa fa-print'
	],
	'options' => [
		'debug' => false,
		'importCSS' => true,
		'importStyle' => false,
		// 'loadCSS' => "path/to/my.css",
		'pageTitle' => "",
		'removeInline' => false,
		'printDelay' => 333,
		'header' => null,
		'formValues' => true,
	]
]);
?>
