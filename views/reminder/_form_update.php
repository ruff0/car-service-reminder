<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\depdrop\DepDrop;
use yii\helpers\Url;

use app\models\Service;

// use yii\jui\DatePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Reminder */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<div class="reminder-form">

  <table>

    <tr>

        <td class="col-md-4">

                <?= $form->field($model, 'customer')->label(false)->textInput([
                'maxlength' => true,
                'placeholder'=>'Cliente', ]) ?>
        </td>

        <td class="col-md-2">

              <?= $form->field($model, 'phone')->label(false)->textInput(['maxlength' => true,
              'placeholder'=>'Telefono',
              ]) ?>
        </td>

        <td class="col-md-3">

                <?= $form->field($model, 'car')->label(false)->textInput([
                'maxlength' => true,
                'placeholder'=>'Automóvil',
              ]) ?>
        </td>

    </tr>

  </table>
  <table>

    <tr>

        <td class="col-md-2">


              <?= $form->field($model, 'service')->label(false)->dropDownList(Service::getServices(), [
                'id'=>'service-id', 'prompt' => 'Servicio de'
              ]); ?>
        </td>

        <td class="col-md-2">

<!--
              <?= $form->field($model, 'permutation')->label(false)->widget(DepDrop::classname(), [
                  'options'=>['prompt' => 'Siguiente servicio en'],
                  // 'data' => Next::getNextList($model->service),

                  'pluginOptions'=>[
                      'depends'=>['service-id'],
                      'placeholder'=>'Siguiente servicio en',
                      'url'=>Url::to(['reminder/next'])
                  ]
              ]); ?> -->

        </td>
    </tr>

  </table>

    <table>
      <tr>
          <td class="col-md-2">
                <?= $form->field($model, 'next_date')->widget(DatePicker::classname(), [
                  'options' => ['placeholder' => 'Siguiente Servicio'],
                  'pluginOptions' => [
                      'autoclose'=>true,
                      'format' => 'yyyy-mm-dd'
                  ]
              ]); ?>
          </td>
          <td class="col-md-4">
                <?= $form->field($model, 'anotation')->label(false)->textInput([
                'maxlength' => true,
                'placeholder'=>'Comentarios',
              ]) ?>
          </td>
      </tr>
    </table>

    <div class="form-group pull-right">
        <?= Html::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
