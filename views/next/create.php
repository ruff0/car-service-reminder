<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Next */

$this->title = 'Create Next';
$this->params['breadcrumbs'][] = ['label' => 'Nexts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
