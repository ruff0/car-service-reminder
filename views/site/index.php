<?php

/* @var $this yii\web\View */

$this->title = 'REFACCIONARIA Y SERVICIOS JUÁREZ';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>REFACCIONARIA Y SERVICIOS JUÁREZ</h2>

        <p class="lead">Sistema para Recordatorios de los Servicios.</p>

        <p><a class="btn btn-lg btn-info" href="index.php?r=reminder/create">Registrar Servicio</a></p>

        <p><a class="btn btn-lg btn-primary" href="index.php?r=reminder/indextoday">Recordatorios para Hoy</a></p>

        <p><a class="btn btn-lg btn-default" href="index.php?r=reminder/create">Recordatorios de la Semana</a></p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <!-- <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p> -->

                <!-- <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Documentación &raquo;</a></p> -->
            </div>

        </div>

    </div>
</div>
