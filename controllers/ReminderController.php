<?php

namespace app\controllers;

use Yii;
use app\models\Reminder;
use app\models\ReminderSearch;
use app\models\Next;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\base\Model;

class ReminderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ReminderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort(['defaultOrder' => ['id'=>SORT_DESC]]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Reminder();
        $today = date('Y-m-d');
        $model->load(Yii::$app->request->post());
        $model->next_date = date('Y-m-d', strtotime("+$model->permutation months", strtotime($today)));
        $model->date = $today;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $today = date('Y-m-d');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Reminder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionNext() {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $service_id = $parents[0];
                $out = Next::getNextList($service_id);

                return json_encode(['output'=>$out, 'selected'=>'']);

            }
        }
        return json_encode(['output'=>'', 'selected'=>'']);
    }

        public function actionIndextoday()
        {
          $model = new Reminder;
          $today = date('Y-m-d');

          // $expression = new Expression('NOW()');
          $searchModel = ReminderSearch::find()->andFilterWhere(['like', 'next_date', $today]);

          $dataProvider = new ActiveDataProvider(['query' =>$searchModel,
          'sort' => ['attributes' => [

                             'id' => [

                                'default' => SORT_DESC
                             ],
                        ],],

                      ]);

          if(Yii::$app->request->post('hasEditable'))
          {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if ($model->load($post))
            {
              $value = $model->status;
              return ['output'=> $value, 'message' => ''];

            }
            else {
              return ['otuput' => '', 'message' => ''];
            }

          }

          return $this->render('indextoday', [
              'searchModel' => $searchModel,
              'dataProvider' => $dataProvider,
              'today' =>  $today,
              'model' => $model,
          ]);
        }


        public function actionIndexweek()
        {
          $model = new Reminder;
          $today = date('Y-m-d');
          $week = date('Y-m-d', strtotime("+1 week", strtotime($today)));
          $searchModel = ReminderSearch::find()->andFilterWhere(['between', 'next_date', $today, $week]);
          $dataProvider = new ActiveDataProvider(['query' =>$searchModel,
          'sort' => ['attributes' => ['id' => ['default' => SORT_DESC]]]]);
          if(Yii::$app->request->post('hasEditable'))
          {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if ($model->load($post))
            {
              $value = $model->status;
              return ['output'=> $value, 'message' => ''];
            }
            else {
              return ['otuput' => '', 'message' => ''];
            }

          }
          return $this->render('indexweek', [
              'searchModel' => $searchModel,
              'dataProvider' => $dataProvider,
              'today' =>  $today,
              'week' =>  $week,
              'model' => $model,
          ]);
        }


        public function actionCalendar()
        {
            $searchModel = new ReminderSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->setSort(['defaultOrder' => ['id'=>SORT_DESC]]);
            return $this->render('calendar', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }


}
