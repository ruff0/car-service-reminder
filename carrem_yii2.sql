-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 14, 2017 at 04:25 AM
-- Server version: 5.5.58-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `carrem_yii2`
--

-- --------------------------------------------------------

--
-- Table structure for table `next`
--

CREATE TABLE IF NOT EXISTS `next` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `months` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `name` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Dumping data for table `next`
--

INSERT INTO `next` (`id`, `months`, `service_id`, `name`) VALUES
(1, 6, 1, '6 meses'),
(2, 5, 1, '5 meses'),
(3, 4, 1, '4 meses'),
(4, 9, 2, '9 meses'),
(5, 10, 2, '10 meses'),
(6, 11, 2, '11 meses'),
(7, 10, 3, '10 meses'),
(8, 11, 3, '11 meses'),
(9, 12, 3, '12 meses');

-- --------------------------------------------------------

--
-- Table structure for table `reminder`
--

CREATE TABLE IF NOT EXISTS `reminder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `car` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `service` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permutation` int(11) DEFAULT NULL,
  `next_date` date DEFAULT NULL,
  `test` int(11) DEFAULT NULL,
  `anotation` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=78 ;

--
-- Dumping data for table `reminder`
--

INSERT INTO `reminder` (`id`, `customer`, `car`, `phone`, `service`, `date`, `permutation`, `next_date`, `test`, `anotation`) VALUES
(70, '2', '2', '2', '1', '2017-12-13', 6, '2017-12-16', NULL, '2'),
(71, '', '3', '', '1', '2017-12-14', 6, '2018-07-06', NULL, '3'),
(73, '23', '23', '32', '1', '2017-12-14', 5, '2018-05-14', NULL, '23'),
(74, '43', '3', '34', '2', '2017-12-14', 9, '2018-09-14', NULL, '34'),
(75, '2', '2', '21', '2', '2017-12-14', NULL, '1970-01-01', NULL, '12'),
(76, 'gf', '34', '34', '1', '2017-12-14', 9, '2018-09-14', NULL, '43'),
(77, '4', '43', '34', '2', '2017-12-14', 6, '2017-12-14', NULL, '43');

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `service`) VALUES
(1, 'Cambio de Aceite'),
(2, 'Afinación'),
(3, 'Lavado de Inyectores');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
