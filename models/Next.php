<?php

namespace app\models;
use app\models\Service;
use Yii;

/**
 * This is the model class for table "next".
 *
 * @property integer $id
 * @property integer $months
 * @property integer $service_id
 */
class Next extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'next';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['months', 'service_id'], 'required'],
            [['months', 'service_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'months' => 'Months',
            'service_id' => 'Service ID',
        ];
    }

    public function getService()
    {
      return $this->hasOne(Service::calssname(),['id' => 'service_id']);
    }

    public static function getNextList($service_id)
    {
      $next = self::find()->select(['months AS id','name AS name'])->where(['service_id' => $service_id])->asArray()->all();
      return $next;
    }
}
