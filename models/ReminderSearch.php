<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Reminder;

/**
 * ReminderSearch represents the model behind the search form about `app\models\Reminder`.
 */
class ReminderSearch extends Reminder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'permutation'], 'integer'],
            [['anotation', 'customer', 'car', 'phone', 'service',
            // 'date', 'next_date'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Reminder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'next_date' => $this->next_date,
        ]);

        $query->andFilterWhere(['like', 'customer', $this->customer])
            ->andFilterWhere(['like', 'car', $this->car])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'service', $this->service])
            ->andFilterWhere(['like', 'anotation', $this->anotation]);

        return $dataProvider;
    }
}
