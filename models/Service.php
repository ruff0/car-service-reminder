<?php

namespace app\models;
use app\models\Next;


use Yii;

/**
 * This is the model class for table "service".
 *
 * @property integer $id
 * @property string $service
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service'], 'required'],
            [['service'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service' => 'Service',
        ];
    }

    public static function getServices()
    {
      return self::find()->select(['service AS name', 'id AS id'])->indexBy('id')->column();
    }


    public function getNexts()
    {
      return $this->hasMany(Next::className(),['service_id' => 'id']);
    }
}
