<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reminder".
 *
 * @property integer $id
 * @property string $customer
 * @property string $car
 * @property string $phone
 * @property string $service
 * @property string $date
 * @property string $next_date
 */
class Reminder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reminder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['car'], 'required'],
            [['permutation', 'test'], 'integer'],
            // [['next_date'], 'safe'],
            [['date', 'customer', 'car', 'phone', 'service', 'anotation'], 'string', 'max' => 60],
            [['next_date'], 'default', 'value' => null],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer' => 'Cliente',
            'car' => 'Automóvil',
            'phone' => 'Teléfono',
            'service' => 'Servicio',
            'date' => 'Fecha',
            'next_date' => 'Siguiente Servicio',
            'permutation' => '',
            'test' => '',
            'anotation' => 'Comentarios',
        ];
    }
}
